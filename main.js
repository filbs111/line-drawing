var gridsquaresize = 32;
var canvassurround = 256;
var canvascentresize = 1024;
var canvassize = canvascentresize + 2*canvassurround;
var ctx;

var lineCoords = {startx:50, starty:50, endx:100, endy:100}

var griddivs = canvascentresize/gridsquaresize;

var gridOccupied = (function(){

    var occupancy = new Array(griddivs*griddivs*griddivs);
    var idx=0;
    for (var ii=0;ii<griddivs;ii++){
        for(var jj=0;jj<griddivs;jj++){
            for(var kk=0;kk<griddivs;kk++){
                var xoffs = ii-7.5;
                var yoffs = jj-7.5;
                var zoffs = kk-7.5
                var nearnessOne = (xoffs*xoffs + yoffs*yoffs + zoffs*zoffs)/25;   //rad 5
                var xoffs2 = ii-17.5;
                var yoffs2 = jj-20;
                var zoffs2 = kk-10;
                var nearnessTwo = (xoffs2*xoffs2 + yoffs2*yoffs2 + zoffs2*zoffs2)/64; //rad 8

                    //something like metaballs
                occupancy[idx++]= 1/nearnessOne  +  1/nearnessTwo > 1 ? true: false;
            }
        }
    }

    return function(xx,yy,zz){
        //TODO ensure don't call outside range
        if (xx<0 || xx>=griddivs){return false;}
        if (yy<0 || yy>=griddivs){return false;}
        if (zz<0 || zz>=griddivs){return false;}

        return occupancy[xx + yy*griddivs + zz*griddivs*griddivs];
    }
})();

function onload(){
    console.log("loaded page. in onload()");

    //line drawing program!
    //with view to using for voxel rendering, collision detection.

    drawProjections();

    var canvas = document.getElementById("mycanvas");
    canvas.width = canvassize;
    canvas.height = canvassize;

    ctx = canvas.getContext("2d");
    ctx.strokeStyle="#fff";

    document.addEventListener("mousedown", e => {
        var rect = e.target.getBoundingClientRect();
        //note relative to grid (offset from canvas 0 by canvassurround)
        lineCoords.endx = e.clientX - rect.left - canvassurround;
        lineCoords.endy = e.clientY - rect.top - canvassurround;
    });

    requestAnimationFrame(iterateMovement);
}

var iterateMovement = (function(){
    var lastTime = Date.now();
    return function(timestamp){
        requestAnimationFrame(iterateMovement);

        var timeElapsed = timestamp - lastTime;
        var moveMultiplier = timeElapsed / 10;
        lastTime = timestamp;

        var moveAmountX = moveMultiplier*( keyThing.keystate(68) - keyThing.keystate(65));
        var moveAmountY = moveMultiplier*( keyThing.keystate(83) - keyThing.keystate(87));
        lineCoords.startx += moveAmountX;
        lineCoords.starty += moveAmountY;

        redraw();

        var angle = document.getElementById("rotation").value;
        var elevation = document.getElementById("elevation").value;
        var zoom = document.getElementById("zoom").value;
        drawAngledProjection(angle, elevation, zoom); //very slow!
    }
})();


var sliceNumber = 15;

function redraw(){

    //clear screen
    ctx.fillStyle = "#000";
    ctx.fillRect(0,0,canvassize,canvassize);

    ctx.fillStyle = "#f00";

    //draw occupancy
    for (var ii=0;ii<griddivs;ii++){
        for(var jj=0;jj<griddivs;jj++){
            ctx.fillStyle = gridOccupied(ii,jj, sliceNumber) ? "rgba(0,100,100,0.2)" : "rgba(100,100,0,0.2)";
            ctx.fillRect(canvassurround+ii*gridsquaresize, canvassurround+jj*gridsquaresize, gridsquaresize, gridsquaresize);
        }
    }


    //draw grid lines 
    for (aa=0;aa<=canvascentresize;aa+=gridsquaresize){
        ctx.fillRect(canvassurround,canvassurround+aa,canvascentresize,1);
        ctx.fillRect(canvassurround+aa,canvassurround,1,canvascentresize);
    }


    //draw something at start coords (TODO use for line draw)
    ctx.fillRect(canvassurround+ lineCoords.startx - 5, canvassurround+ lineCoords.starty - 5, 10, 10);
    var xpos = lineCoords.endx;
    var ypos = lineCoords.endy;

    ctx.fillStyle = "rgba(0,0,255,0.2)";

    traceLine(lineCoords, true);

    //line from start to end.
    ctx.beginPath();
    ctx.moveTo(canvassurround+ lineCoords.startx,canvassurround+ lineCoords.starty);
    ctx.lineTo(canvassurround+ xpos,canvassurround+ ypos);
    ctx.closePath();
    ctx.stroke();

//===========

    //crossing point of line between points with cell diagonals.
    //note working below is rough - hacked in adjustments after. yet to explain.

    //find set of diagonals to find crossing point for.
    // suppose gridx+gridy = p ... q (integers).
    // line equation : 
    //  x = xstart + t*vx   (1)
    //  y = ystart + t*vy   (2)

    // for line r:  x + y = r
    // y = r - x
    // sub into (2)
    // r - x = ystart + t*vy
    
    // would like to find x-y, which will allow finding grid square.
    // should eliminate or find t. note that vx or vy might be zero.
    // (1) + (2)
    // 2r = xstart + t*vx + ystart + t*vy
    // 2r - xstart - ystart = t ( vx + vy )
    // t =  ( 2r - xstart - ystart ) / (vx + vy)        (3)
    
    // now to find x-y = (1) - (2)
    //  ( xstart + t*vx ) - ( ystart + t*vy )
    // (xstart - ystart) + t ( vx - vy )
    // sub in (3)
    // (xstart - ystart) + ( 2r - xstart - ystart ) * ( vx - vy )/(vx + vy)
    // ....


    // simpler method  : 
    // just find t for a given diagonal, use this to get x and y square. if any possibility of picking wrong square( out by 1 ),
    //due to float rounding etc, and decide it matters, might guarantee is on diagonal line (so no squares counted twice), by finding only x,
    // then finding y = r-x, or vice versa

}

function fillSquare(gridx, gridy, occupied){
    ctx.fillStyle = occupied ? "rgba(255,0,0,0.2)": "rgba(0,255,0,0.2)";
    ctx.fillRect(canvassurround + gridx*gridsquaresize, canvassurround + gridy*gridsquaresize, gridsquaresize,gridsquaresize);
}

function drawDot(x,y){
    ctx.fillStyle = "#fff";
    ctx.fillRect(canvassurround+ x-2,canvassurround+ y-2,5,5);
}

function traceLine(lineCoords, drawStuff){
    var squareDraw = drawStuff? fillSquare: (x,y,o) => -1;
    var dotDraw = drawStuff? drawDot: (x,y) => -1;

    if (!lineCoords.startz){
        lineCoords.startz=sliceNumber*gridsquaresize;   //+0.5??
    }
    if (!lineCoords.endz){
        lineCoords.endz=sliceNumber*gridsquaresize;
    }

    //find initial grid square.
    //TODO is this outside grid?
    var startgridx = Math.floor( lineCoords.startx / gridsquaresize );
    var startgridy = Math.floor( lineCoords.starty / gridsquaresize );
    var startgridz = Math.floor( lineCoords.startz / gridsquaresize );
    squareDraw( startgridx, startgridy );

    var endgridx = Math.floor( lineCoords.endx / gridsquaresize );
    var endgridy = Math.floor( lineCoords.endy / gridsquaresize );
    var endgridz = Math.floor( lineCoords.endz / gridsquaresize );
    // fillSquare( endgridx, endgridy );

    //cap grid squares - still some erroneous checks of squares outside of grid, but better than nothing
    //note if guarantee that start, end points are within grid, this is unnecessary
    startgridx = Math.min(Math.max(startgridx, 0),griddivs-1);
    startgridy = Math.min(Math.max(startgridy, 0),griddivs-1);
    startgridz = Math.min(Math.max(startgridz, 0),griddivs-1);
    endgridx = Math.min(Math.max(endgridx, 0),griddivs-1);
    endgridy = Math.min(Math.max(endgridy, 0),griddivs-1);
    endgridz = Math.min(Math.max(endgridz, 0),griddivs-1);

    var startXPlusYPlusZ = lineCoords.startx + lineCoords.starty + lineCoords.startz;
    var endXPlusYPlusZ = lineCoords.endx + lineCoords.endy + lineCoords.endz;
    var startGridXPlusYPlusZ = startgridx + startgridy + startgridz;
    var endGridXPlusYPlusZ = endgridx + endgridy + endgridz;

    //similar to direction vector
    //. TODO find differently for directional line (to avoid possible /0)
    //note only need to find one of these things per line (depends on octant of direction)
    var xmultiplierPPP = (lineCoords.endx - lineCoords.startx) / (endXPlusYPlusZ - startXPlusYPlusZ);
    var ymultiplierPPP = (lineCoords.endy - lineCoords.starty) / (endXPlusYPlusZ - startXPlusYPlusZ);
    var zmultiplierPPP = (lineCoords.endz - lineCoords.startz) / (endXPlusYPlusZ - startXPlusYPlusZ);

    //used for x*y=1, x*z=-1 octants. (TODO generalise)
    var startXPlusYMinusZ = lineCoords.startx + lineCoords.starty - lineCoords.startz;
    var endXPlusYMinusZ = lineCoords.endx + lineCoords.endy - lineCoords.endz;
    var startGridXPlusYMinusZ = startgridx + startgridy - startgridz;
    var endGridXPlusYMinusZ = endgridx + endgridy - endgridz;

    var xmultiplierPPM = (lineCoords.endx - lineCoords.startx) / (endXPlusYMinusZ - startXPlusYMinusZ);
    var ymultiplierPPM = (lineCoords.endy - lineCoords.starty) / (endXPlusYMinusZ - startXPlusYMinusZ);
    var zmultiplierPPM = (lineCoords.endz - lineCoords.startz) / (endXPlusYMinusZ - startXPlusYMinusZ);


    //use ifs for different quadrants. TODO generalise.
    //TODO check ii for loop ranges for 3D.
    if (lineCoords.startx < lineCoords.endx){
        if (lineCoords.starty < lineCoords.endy){
            if (lineCoords.startz < lineCoords.endz){
                //+x, +y, +z directions
                for (var ii=startGridXPlusYPlusZ+1;ii<=endGridXPlusYPlusZ+1;ii+=0.5){    //note +1s hacked
                    //hacked for +0.33 because assumption of diagonal crossings doesn't hold in 3d. (doesn't fully fix problem)
                    //result looks hairy (axis aligned looks OK though)
                    //TODO find way to, for each diagonal layer of cubes, identify which is intersected.
                    // harder than 2d case, where simple diagonal cuts through line of diagonal squares.
                    // from POV of ray, sucessive layers look like hexagonal grids - but not regular hexagons (unless ray along diagonal)
                    // consider superimposing view of each diagonal layer - see problem equivalent to traversing a view of one layer
                    // with evenly spaced samples (one per layer) - how to quickly detect which hex each sample is in?
                    // https://www.redblobgames.com/grids/hexagons/#rounding  - but much easier for regular hex 
                    // (or that can be transformed to regular)
                    // see also https://www.redblobgames.com/grids/line-drawing.html
                    // perhaps can describe view of 1 diagonal cube layer by 2 or 3 triangular grids.... 
                    // or just give up and use amanatides woo algo

                    // t =  ( 2r - xstart - ystart ) / (vx + vy)        (3)
                    var t = (ii*gridsquaresize - startXPlusYPlusZ); 
                    var x = lineCoords.startx + t*xmultiplierPPP;
                    var y = lineCoords.starty + t*ymultiplierPPP;
                    var z = lineCoords.startz + t*zmultiplierPPP;

                    var gridx =  Math.floor( x / gridsquaresize );
                    var gridy =  Math.floor( y / gridsquaresize );
                    var gridz =  Math.floor( z / gridsquaresize );
                    // var gridy = ii - gridx -1;       
                        //TODO clever business to guarantee 1 cube per diagonal plane. (ie x+y+z = plane number)
                        //for now do simple, because less to go wrong.

                    var occupied = gridOccupied(gridx,gridy, gridz);
                    squareDraw(gridx, gridy, occupied);
                    dotDraw(x,y);

                    if (occupied){return [gridx,gridy,gridz];}
                }
            }else{
                // +x, +y, -z directions
                for (var ii=startGridXPlusYMinusZ+1;ii<=endGridXPlusYMinusZ+1;ii+=0.5){    //TODO check range
                    // t =  ( 2r - xstart - ystart ) / (vx + vy)        (3)
                    var t = (ii*gridsquaresize - startXPlusYMinusZ); 
                    var x = lineCoords.startx + t*xmultiplierPPM;
                    var y = lineCoords.starty + t*ymultiplierPPM;
                    var z = lineCoords.startz + t*zmultiplierPPM;

                    var gridx =  Math.floor( x / gridsquaresize ) ;
                    var gridy =  Math.floor( y / gridsquaresize ) ;
                    var gridz =  Math.floor( z / gridsquaresize ) ;
                    // var gridy = ii - gridx -1;       
                        //TODO clever business to guarantee 1 cube per diagonal plane. (ie x+y+z = plane number)
                        //for now do simple, because less to go wrong.

                    var occupied = gridOccupied(gridx,gridy, gridz);
                    squareDraw(gridx, gridy, occupied);
                    dotDraw(x,y);

                    if (occupied){return [gridx,gridy,gridz];}
                }
            }
        }else{
            // //+x, -y directions
            // for (var ii=startGridXMinusY;ii<=endGridXMinusY;ii++){
            //     // t =  ( 2r - xstart - ystart ) / (vx + vy)        (3)
            //     var t = (ii*gridsquaresize - startXMinusY); 
            //     var x = lineCoords.startx + t*xmultiplierMinus;
            //     var y = lineCoords.starty + t*ymultiplierMinus;
            //     var z = lineCoords.startz + t*zmultiplierMinus;

            //     var gridx =  Math.floor( x / gridsquaresize ) ;
            //     var gridy =  Math.floor( y / gridsquaresize ) ;
            //     var gridz =  Math.floor( z / gridsquaresize ) ;
            //     // var gridy = - ii + gridx;

            //     var occupied = gridOccupied(gridx,gridy, gridz);
            //     ctx.fillStyle = occupied ? "rgba(255,0,0,0.2)": "rgba(0,255,0,0.2)";
            //     squareDraw(gridx, gridy);
            //     dotDraw(x,y);

            //     if (occupied){return [gridx,gridy];}
            // }
        }
    }else{
        if (lineCoords.starty < lineCoords.endy){
        //     //-x, +y directions
        //     for (var ii=startGridXMinusY;ii>=endGridXMinusY;ii--){
        //         // t =  ( 2r - xstart - ystart ) / (vx + vy)        (3)
        //         var t = (ii*gridsquaresize - startXMinusY); 
        //         var x = lineCoords.startx + t*xmultiplierMinus;
        //         var y = lineCoords.starty + t*ymultiplierMinus;
        //         var z = lineCoords.startz + t*zmultiplierMinus;

        //         var gridx =  Math.floor( x / gridsquaresize ) ;
        //         var gridy =  Math.floor( y / gridsquaresize ) ;
        //         var gridz =  Math.floor( z / gridsquaresize ) ;
        //         // var gridy = - ii + gridx;

        //         var occupied = gridOccupied(gridx,gridy, gridz);
        //         ctx.fillStyle = occupied ? "rgba(255,0,0,0.2)": "rgba(0,255,0,0.2)";
        //         squareDraw(gridx, gridy);
        //         dotDraw(x,y);

        //         if (occupied){return [gridx,gridy];}
        //     }
        }else{
            //-x, -y directions
            // for (var ii=startGridXPlusY+1;ii>=endGridXPlusY+1;ii--){
            //     // t =  ( 2r - xstart - ystart ) / (vx + vy)        (3)
            //     var t = (ii*gridsquaresize - startXPlusY); 
            //     var x = lineCoords.startx + t*xmultiplier;
            //     var y = lineCoords.starty + t*ymultiplier;
            //     var z = lineCoords.startz + t*zmultiplier; 

            //     var gridx =  Math.floor( x / gridsquaresize ) ;
            //     var gridy =  Math.floor( y / gridsquaresize ) ;
            //     var gridz =  Math.floor( z / gridsquaresize ) ;
            //     // var gridy = ii - gridx -1;

            //     var occupied = gridOccupied(gridx,gridy, gridz);
            //     ctx.fillStyle = occupied ? "rgba(255,0,0,0.2)": "rgba(0,255,0,0.2)";
            //     squareDraw(gridx, gridy);
            //     dotDraw(x,y);

            //     if (occupied){return [gridx,gridy];}
            // }
        }
    }
    return -1;  //no collision
}