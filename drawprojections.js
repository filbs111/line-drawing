function drawProjections(){

    var pcanvas = document.getElementById("projectionscanvas");

    //size 4x3 times a square, plus some filler
    var fillerspacing = 5;
    var step = fillerspacing + griddivs;

    var totalsizex = fillerspacing + 4*step;
    var totalsizey = fillerspacing + 3*step;

    //fill where projections will be
    pcanvas.width = totalsizex;
    pcanvas.height = totalsizey;

    var pctx = pcanvas.getContext("2d");

    pctx.fillStyle = "black";
    pctx.fillRect(fillerspacing, fillerspacing +step, griddivs, griddivs);   // left

    pctx.fillRect(fillerspacing +step, fillerspacing, griddivs, griddivs);  //top
    pctx.fillRect(fillerspacing +step, fillerspacing +step, griddivs, griddivs);   //back
    pctx.fillRect(fillerspacing +step, fillerspacing +2*step, griddivs, griddivs); //bottom

    pctx.fillRect(fillerspacing +2*step, fillerspacing +step, griddivs, griddivs);  //right

    pctx.fillRect(fillerspacing +3*step, fillerspacing +step, griddivs, griddivs);  //front

    //TODO make efficient by imagedata?

    //draw back. +x to right, +y downward, +z into page. therefore for each x,y, step through z in +ve direction until hit something
    for (var xx=0;xx<griddivs;xx++){
        for (var yy=0;yy<griddivs;yy++){
            var hit=false;
            for (var zz=0;zz<griddivs;zz++){
                if (gridOccupied(xx,yy,zz)){
                    hit=true;
                    pctx.fillStyle = "rgba(" + Math.floor(xx*255/griddivs) +","+ Math.floor(yy*255/griddivs)
                    + ","+ Math.floor(zz*255/griddivs) +",1)";
                    break;
                }
            }
            if (hit){
                pctx.fillRect(fillerspacing+step +xx, fillerspacing +step+yy,1,1);
            }
        }
    }

    //draw front. +x to left, +y downward, +z out of page. therefore for each x,y, step through z in -ve direction until hit something
    for (var xx=0;xx<griddivs;xx++){
        for (var yy=0;yy<griddivs;yy++){
            var hit=false;
            for (var zz=griddivs-1;zz>=0;zz--){
                if (gridOccupied(xx,yy,zz)){
                    hit=true;
                    pctx.fillStyle = "rgba(" + Math.floor(xx*255/griddivs) +","+ Math.floor(yy*255/griddivs)
                    + ","+ Math.floor(zz*255/griddivs) +",1)";
                    break;
                }
            }
            if (hit){
                pctx.fillRect(fillerspacing+3*step + (griddivs-xx), fillerspacing +step+yy,1,1);
            }
        }
    }

    //draw left. +z to left, +y downward, +x into page. therefore for each z,y, step through x in +ve direction until hit something
    for (var zz=griddivs-1;zz>=0;zz--){
        for (var yy=0;yy<griddivs;yy++){
            var hit=false;
            for (var xx=0;xx<griddivs;xx++){
                if (gridOccupied(xx,yy,zz)){
                    hit=true;
                    pctx.fillStyle = "rgba(" + Math.floor(xx*255/griddivs) +","+ Math.floor(yy*255/griddivs)
                    + ","+ Math.floor(zz*255/griddivs) +",1)";
                    break;
                }
            }
            if (hit){
                pctx.fillRect(fillerspacing + (griddivs-zz), fillerspacing +step+yy,1,1);
            }
        }
    }

    //draw right. +z to right, +y downward, +x out of page. therefore for each z,y, step through x in -ve direction until hit something
    for (var zz=0;zz<griddivs;zz++){
        for (var yy=0;yy<griddivs;yy++){
            var hit=false;
            for (var xx=griddivs-1;xx>=0;xx--){
                if (gridOccupied(xx,yy,zz)){
                    hit=true;
                    pctx.fillStyle = "rgba(" + Math.floor(xx*255/griddivs) +","+ Math.floor(yy*255/griddivs)
                    + ","+ Math.floor(zz*255/griddivs) +",1)";
                    break;
                }
            }
            if (hit){
                pctx.fillRect(fillerspacing+2*step + zz, fillerspacing +step+yy,1,1);
            }
        }
    }

     //draw top. +x to the right, +z upward, +y into page. therefore for each x,z, step through y in -ve direction until hit something
     for (var xx=0;xx<griddivs;xx++){
        for (var zz=0;zz<griddivs;zz++){
            var hit=false;
            for (var yy=0;yy<griddivs;yy++){
                if (gridOccupied(xx,yy,zz)){
                    hit=true;
                    pctx.fillStyle = "rgba(" + Math.floor(xx*255/griddivs) +","+ Math.floor(yy*255/griddivs)
                    + ","+ Math.floor(zz*255/griddivs) +",1)";
                    break;
                }
            }
            if (hit){
                pctx.fillRect(fillerspacing+ step + xx, fillerspacing + (griddivs-zz),1,1);
            }
        }
    }

    //draw bottom. +x to the right, +z downward, +y out of page. therefore for each x,z, step through y in +ve direction until hit something
    for (var xx=0;xx<griddivs;xx++){
        for (var zz=0;zz<griddivs;zz++){
            var hit=false;
            for (var yy=griddivs-1;yy>=0;yy--){
                if (gridOccupied(xx,yy,zz)){
                    hit=true;
                    pctx.fillStyle = "rgba(" + Math.floor(xx*255/griddivs) +","+ Math.floor(yy*255/griddivs)
                    + ","+ Math.floor(zz*255/griddivs) +",1)";
                    break;
                }
            }
            if (hit){
                pctx.fillRect(fillerspacing+ step + xx, fillerspacing +2*step + zz,1,1);
            }
        }
    }

    console.log("in drawProjections. griddivs = " + griddivs);
}


var drawAngledProjection = (function(){

    var scaleup = 3;

    var rvcanvas = document.getElementById("rotatedviewcanvas");
    rvcanvas.width = griddivs*2*scaleup;
    rvcanvas.height = griddivs*2*scaleup;
    var rvctx = rvcanvas.getContext("2d");
    var idata = rvctx.createImageData(rvcanvas.width, rvcanvas.height);
    var idatadata = idata.data;

    return function(angle, elevation, zoom){

        var totaldimension = gridsquaresize*griddivs;
        var centre = totaldimension/2;
        var sa = Math.sin(angle);
        var ca = Math.cos(angle);

        var ce = Math.cos(elevation);
        var se = Math.sin(elevation);

        var viewvectorx = ce*ca;
        var viewvectory = ce*sa;
        var viewvectorz = se;

        var horizvectorx = -sa;
        var horizvectory = ca;
        var horizvectorz = 0;

        var vertvectorx = -se*ca;
        var vertvectory = -se*sa;
        var vertvectorz = ce;

        
        var zoomscale = zoom;

        var startcentrex =  centre - (viewvectorx+(horizvectorx+vertvectorx)/zoomscale)*totaldimension;
        var startcentrey =  centre - (viewvectory+(horizvectory+vertvectory)/zoomscale)*totaldimension;
        var startcentrez =  centre - (viewvectorz+(horizvectorz+vertvectorz)/zoomscale)*totaldimension;

        var endcentrex =    startcentrex + 2*totaldimension*viewvectorx;
        var endcentrey =    startcentrey + 2*totaldimension*viewvectory;
        var endcentrez =    startcentrez + 2*totaldimension*viewvectorz;

        var multiplier = 1/(scaleup*zoomscale);

        var horizstepx = multiplier*gridsquaresize*horizvectorx;
        var horizstepy = multiplier*gridsquaresize*horizvectory;
        var horizstepz = multiplier*gridsquaresize*horizvectorz;   //=0

        var vertstepx = multiplier*gridsquaresize*vertvectorx;
        var vertstepy = multiplier*gridsquaresize*vertvectory;
        var vertstepz = multiplier*gridsquaresize*vertvectorz;

        for (var jj=0;jj<rvcanvas.height;jj++){
            for (var ii=0;ii<rvcanvas.width;ii++){
                var linestartx = startcentrex+ii*horizstepx + jj*vertstepx;
                var linestarty = startcentrey+ii*horizstepy + jj*vertstepy;
                var linestartz = startcentrez+ii*horizstepz + jj*vertstepz;
                var lineendx = endcentrex+ii*horizstepx + jj*vertstepx;
                var lineendy = endcentrey+ii*horizstepy + jj*vertstepy;
                var lineendz = endcentrez+ii*horizstepz + jj*vertstepz;
                var lineCoords = {startx:linestartx, starty:linestarty, startz:linestartz, endx:lineendx, endy:lineendy, endz:lineendz}
                var collision = traceLine(lineCoords, false);

                var idx4 = 4*(ii+jj*rvcanvas.width);
                if (collision!=-1){
                    idatadata[idx4 ] = Math.floor(collision[0]*255/griddivs);
                    idatadata[idx4+1 ] = Math.floor(collision[1]*255/griddivs);
                    idatadata[idx4+2 ] = Math.floor(jj*255/griddivs);
                    idatadata[idx4+3 ] = 255;
                }else{
                    idatadata[idx4 ] = 0;
                    idatadata[idx4+1 ] = 0;
                    idatadata[idx4+2 ] = 0;
                    idatadata[idx4+3 ] = 255;
                }
            }
        }

        rvctx.putImageData(idata, 0,0);
    }
})();

