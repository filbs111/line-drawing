raycasting. hunch: if ray starts from centre of a grid square, and space samples by 1 / (1/deltaX + 1/deltaY + 1/deltaZ ... ) then samples will land one in each of the grid squares intersected by the ray.
 ( for grid size 1, ray of length L, passes L/deltaX grid squares, where deltaX = squares crossed by unit ray. -> total crossings = L (1/deltaX + 1/deltaY + 1/deltaZ ... )
expect can do similar for a ray starting at some arbitrary point, but with the 1st sample not at that point. how to figure out? 

how to modify for not starting at centre of a grid square?
	if line goes through the centre of a the grid square that starts in, offset first sample to that point. if line goes through corner of grid square starts in, offset first sample to that point.

can see by inspection (and post hoc rationalisation!) that each dot is on a diagonal line through grid squares!
might ensure that get a sample on each diagonal line, and each diagonal line of grid squares


this works for 2d, but falls over for 3d. see notes in main.js


TODO
---
3d version - fix line tracing - diagonal cube layer collision detection? or just use amanatides woo
add some white lines to draw box around rotatable cube in view. add writing to sides with text???
generalise quadrants/octants.
avoid all redundant tests outside of cube - eg by altering start, end co-ords to within grid
avoid division by something that might be zero.
gpu shader version.
heirarchical tracing?
cube distance field version? https://medium.com/@calebleak/raymarching-voxel-rendering-58018201d9d6
animated demo - something like a lava lamp...

hybrid voxel/deferred? like with point lights, can have some rasterised screen area which is influenced by some object. eg do whole screen to generate buffers for environment. then to add ambient occlusion, radiosity influence etc of some small mobile object, render for some volume around this object. can either look up something from 3d texture for object, or do raytracing/raymarching for the object...

idea to speed up: "bunched rays" - how to do? guess can have low res version that terminates for larger box size (~pixel size). then can, for higher res version, figure out something from surrounding low-res points.

for a gf2 3d game - could have some hybrid renderer where use a voxel shader with different texture for each grid square (in gf2, 16x16 - could be 16x16x16 (=64x64) or 32x32x32 ). render each block to screen with separate calls. or, render each block of the same type as single call, pass in position of explosion spheres to each instance. (would want some way to limit/combine number of spheres)
or, coloured texture for blocks can be shared, but mask is single texture for all screen.
64^3 = 512^2
256^3 = 4096^2
for an individual block, 16x16x16 = 4096. root = 64. 64x64 
what realistic? gf2 was something like 61x21 IIRC -> 1281. say 61x21x21 for 3d = 26901 blocks. 16x16x16 blocks = 4096 each. say img is 32 bit -> 128 pix per block. 26901x128 = 3443328
root = 1855. therefore plausible to fix mask for entire thing into 2k texture.
can then render instanced for all blocks of same type (and perhaps some blocks that start off looking different but share same 3d colouring can share type), with each instance looking up a different part of the big mask.

sparse voxel octrees. SVO. SVDAG
talk of streaming data off disc (guess don't need detail far from camera) https://www.youtube.com/watch?v=Y9SB49C2t5Q https://www.youtube.com/watch?v=km0DpZUgvbg
lossy compression of DAG: https://www.youtube.com/watch?v=Huo6E7knRgk 
https://www.youtube.com/c/TomMulgrew/videos
https://www.youtube.com/user/kindpotato/videos
https://www.youtube.com/watch?v=u09Oojw2Qpw	mentions something that sounds a bit like raymarching - binary manhattan/chebyshev distance? idea - different voxels depending on camera orientation? (eg if know stepping in +x, can store closest in that hemisphere.
voxel terrain. https://www.youtube.com/watch?v=stqgZTws_Z8 https://voxelterrain.com/ "transvoxel" - appears to be marching cubes at different resolutions, with special patching between resolution changes, using lookup table like with marching cubes.  

nice explanation, seems like cpu renderer/collision checker. good idea to get something like this working before looking at gpu stuff? https://www.youtube.com/watch?v=Cfu01-D4z4U
note could generate voxel data using SDF, compare speed vs SDF ray marching.

https://bcmpinc.wordpress.com/2015/08/09/moving-towards-a-gpu-implementation/

some voxel channels:
https://www.youtube.com/user/ThaRemo1995/videos
https://www.youtube.com/user/MCeperoG/videos
https://www.youtube.com/user/ariusmyst/videos
https://www.youtube.com/channel/UC-d1XBCSmpx3Iy7CJiL87Wg/videos
https://www.youtube.com/channel/UC5W4rOjZnrzn_byJ70qDRJg
https://www.youtube.com/channel/UCM2RhfMLoLqG24e_DYgTQeA/videos

voxel storage compression: https://hbfs.wordpress.com/2011/03/22/compressing-voxel-worlds/	https://hbfs.wordpress.com/2009/04/14/ad-hoc-compression-methods-rle/

voxel cone tracing https://www.youtube.com/watch?v=YI4ya1RVzBY https://www.youtube.com/watch?v=EHaSzQTjlik
